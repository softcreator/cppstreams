#include <deque>
#include <vector>
#include <functional>

// TODO: numbers<int>().from(start/step).to(stop)
// TODO: zip(a, b, c) -> gives tuple
// TODO: then<map>().to<int>(...)
// TODO: then<sort>(...?)[.and_remove_duplicates()]
// TODO: then<reduce>().from(val).by(...)
// TODO: then<count>().all()/.value(val)
// TODO: then<check_that>().all_are(val)/any_is(val)
//
// TODO: then<partition>().to_prefer_first(...)/.to_prefer_last(...)
// TODO: then<pick>().item<0>()/items<0, 2>() with <int...> and get<I>()...
// TODO: then<calc_frequency>()
// TODO: then<remove_duplicates>()

template <class T>
class enumerate_range {
 public:
  typedef decltype(*T()) value_type;

  enumerate_range(T begin, T end) : i_(begin), end_(end) {}
  enumerate_range(enumerate_range&&) = default;
  enumerate_range& operator=(enumerate_range&&) = default;

  template <template <class...> class U, class ...P>
  U<enumerate_range<T>, value_type> then(P&&... args) && {
    return U<enumerate_range<T>, value_type>(
        std::move(*this), std::forward<P>(args)...);
  }

  bool move_next() {
    if (i_ == end_)
      return false;
    i_ = std::next(i_);
    return true;
  }

  const value_type& current() const { return *i_; }

 private:
  T i_;
  T end_;
};

template <class T>
class enumerate_container :
    private enumerate_range<decltype(std::begin(T()))> {
 public:
  typedef decltype(std::begin(T())) iterator_type;
  typedef typename std::decay<decltype(*std::begin(T()))>::type value_type;

  enumerate_container(const T& container)
    : enumerate_range(std::begin(container), std::end(container)) {}
  enumerate_container(enumerate_container&&) = default;
  enumerate_container& operator=(enumerate_container&&) = default;

  template <template <class...> class U, class ...P>
  U<enumerate_container<T>, value_type> then(P&&... args) && {
    return U<enumerate_container<T>, value_type>(
        std::move(*this), std::forward<P>(args)...);
  }

  using enumerate_range<iterator_type>::move_next;
  using enumerate_range<iterator_type>::current;
};

template <class T, class Y>
class filter {
 public:
  template <class F>
  class impl {
   public:
    impl(filter&& source, F f, bool compare_with)
        : source_(std::move(source)),
          f_(std::move(f)),
          compare_with_(compare_with) {}
    impl(impl&&) = default;
    impl& operator=(impl&&) = default;

    template <template <class...> class U, class ...P>
    U<impl, Y> then(P&&... args) && {
      return U<impl, Y>(
        std::move(*this), std::forward<P>(args)...);
    }

    bool move_next() {
      result_ = nullptr;
      while (source_.move_next()) {
        if (f_(source_.current()) == compare_with_) {
          result_ = &source_.current();
          break;
        }
      }
      return result_ != nullptr;
    }

    const Y& current() {
      return *result_;
    }

   private:
    filter source_;
    F f_;
    bool compare_with_;
    const Y* result_ = nullptr;
  };

  filter(T&& source)
      : source_(std::move(source)) {}
  filter(filter&&) = default;
  filter& operator=(filter&&) = default;

  template <class F>
  impl<F> to_keep(F&& f) && {
    return impl<F>(std::move(*this), std::move(f), true);
  }
  template <class F>
  impl<F> to_remove(F&& f) && {
    return impl<F>(std::move(*this), std::move(f), false);
  }

  bool move_next() { return source_.move_next(); }
  const Y& current() const { return source_.current(); }

 private:
  T source_;
};

template <class U, class Y>
class push_back {
 public:
  push_back(U&& source)
      : source_(std::move(source)) {}

  template <template <class...> class T, class ...P>
  T<Y, P...> to_new() {
    T<Y, P...> values;
    to(values);
    return values;
  }

  template <class T>
  void to(T& container) {
    while (source_.move_next()) {
      container.push_back(source_.current());
    }
  }

  // TODO: to_iterable() to use in "for (... : iterable) {}"

 private:
  U source_;
};

template <class T>
enumerate_container<T> enumerate(const T& container) {
  return enumerate_container<T>(container);
}

void test() {
  std::deque<int> t{1, 2, 3, 4, 5};
  auto even_numbers = [](int i) { return i % 2 == 0; };
  std::vector<int> v = enumerate(t).then<filter>().to_keep(even_numbers)
                                   .then<push_back>().to_new<std::vector>();
}
